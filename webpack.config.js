module.exports = {
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/,        
        use: [{
          loader: 'ts-loader',
          options: { allowTsInNodeModules: true , transpileOnly: true}
        }],
      },
    ],
  },
}
