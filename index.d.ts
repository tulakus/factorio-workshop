declare namespace Cypress {
    interface Chainable<Subject> {
        buildMine(): Chainable<Subject>; 
        tickGame(): Chainable<Subject>; 
        stoneDeposit(): Chainable<Subject>; 
        getBankDeposit(resource: string): Chainable<Subject>; 
        findMines(): Chainable<Subject>; 
        findFirstStone(): Chainable<Subject>; 
        mineResource(): Chainable<Subject>; 
        findFirstAvailableSourceTile(tile: string): Chainable<Subject>; 
        clickDialogAction(actionName: string): Chainable<Subject>;
        startGame(user: string): Chainable<Subject>;
    }
}