import {Map} from "../pageObjects/map";
import { Factory, ironPlateFactory, steelFactory, steelCasingFactory, rocketShellFactory } from "./Factory";
import {STONE} from "./Resources";

const map = new Map();

export function destroyMine(){
    cy.findMines().then((mines: any) => {
        const mine = mines[0];
        mine.click();
        destroyBuilding();
    })
}

export function restartWorld(){
    cy.request("POST", Cypress.env("baseServerUrl") + "/restart",{})
    cy.tickGame();
}

export function buildStoneMine(){
    buildMine(map.findFirstStoneTile);
}

export function buildIronMine(){
    buildMine(map.findFirstIronTile);
}

export function buildCoalMine(){
    buildMine(map.findFirstCoalTile);
}

function buildMine(findTile: () => Cypress.Chainable){
    waitUntilSourcesAreFullfilled(STONE, 5);
    findTile().click();
    buildMineAction();
    cy.tickGame();
}

export function buildMineAction(){
    return cy.get('#action-BuildMine').click();
}

export function mineStone(stoneCount = 5){
    for (let i = 0; i < stoneCount; i++) {
        cy.findFirstStone().click();
        cy.mineResource();
    }
}

export function buildFactory(factory: Factory){
    waitUntilSourcesAreFullfilled(factory.buildMaterial, factory.buildCost);
    map.findFirstEmptyPlaceTile().click();
    cy.clickDialogAction(factory.name);
    cy.tickGame();
}

export function destroyBuilding(){
    cy.clickDialogAction("DestroyBuilding");
}

export function buildIronFactory(){
    buildFactory(ironPlateFactory);
}

export function buildSteelFactory(){
    buildFactory(steelFactory);
}

export function buildSteelCasingFactory(){
    buildFactory(steelCasingFactory);
}

export function buildRocketShellFactory(){
    buildFactory(rocketShellFactory);
}

function waitUntilSourcesAreFullfilled(resource: string, expectedCount: number){
    var initialStone: number = 0;
    var afterTickStone: number = 0;

    cy.getBankDeposit(resource)
    .then((i: any) => initialStone = i)
    .then( () => {
        if(initialStone < expectedCount){
            cy.tickGame(); 
        }})
    
    cy.getBankDeposit(resource)
    .then((i: any) => afterTickStone = i)
    .then(() => {
        if(afterTickStone < expectedCount){
            waitUntilSourcesAreFullfilled(resource, expectedCount);
        }
    })   
}