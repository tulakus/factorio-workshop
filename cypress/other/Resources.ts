export const IRON_ORE = "IronOre";
export const STONE = "Stone";
export const IRON_PLATE = "IronPlate";
export const STEEL = "Steel";
export const STEEL_CASING = "SteelCasing";