export const STEEL_FACTORY = "BuildSteelFactory";
export const IRON_PLATE_FACTORY = "BuildIronPlateFactory";
export const STEEL_CASING_FACTORY = "BuildSteelCasingFactory";
export const ROCKET_SHELL_FACTORY = "BuildRocketShellFactory";

import {STONE} from "./Resources";

export class Factory{
    buildCost: number;
    buildMaterial: string;
    name: string;
    constructor(name: string, buildMaterial: string = STONE, buildCost: number = 5){
        this.name = name;
        this.buildMaterial = buildMaterial;
        this.buildCost = buildCost;
    }
}

const rocketShellFactory = new Factory(ROCKET_SHELL_FACTORY);
const steelCasingFactory = new Factory(STEEL_CASING_FACTORY);
const steelFactory = new Factory(STEEL_FACTORY);
const ironPlateFactory = new Factory(IRON_PLATE_FACTORY);

export {rocketShellFactory, steelCasingFactory, steelFactory, ironPlateFactory}