import * as tiles from "./mapTiles"

export class Map{
    findFirstIronTile = () => {
        return this.findFirstAvailableSourceTile(tiles.iron);
    }
    
    findFirstStoneTile = () => {
        return this.findFirstAvailableSourceTile(tiles.stone);
    }

    findFirstCoalTile = () => {
        return this.findFirstAvailableSourceTile(tiles.coal);
    }

    findFirstEmptyPlaceTile = () => {
        return this.findFirstAvailableSourceTile(tiles.emptyPlace);
    }

    findFirstAvailableSourceTile = (tileType: string) => {
        return cy.get(`td > div > img[alt="${tileType}"]:first`);
    }
    
    findIronMines = () => {
        return this.findMines(tiles.IRON_MINE_TILE);
    }

    findMines = (tileType: string) => {
        return cy.get(`td > div > img[alt="${tileType}"]`);
    }

    mineResource(){
        this.clickDialogAction("MineResource");
    };
    
    buildMine(){
        this.clickDialogAction("BuildMine");
    };
    
    clickDialogAction(objectName: string){
        cy.get(`#action-${objectName}`).click();
    };
}
