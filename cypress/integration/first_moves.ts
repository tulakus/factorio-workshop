import {Map} from "../pageObjects/map"
import * as MapActions from "../other/MapActions";

context('Mining by hand', () => {
    var  map = new Map();
    beforeEach(() => {
        cy.startGame('Berlin');
        MapActions.restartWorld(); 
        MapActions.mineStone();      
    });

    xit('Display alert when required resource isnt fullfiled', function(){
        
        MapActions.restartWorld(); 

        const stub = cy.stub()  
        cy.on('window:alert', stub);
        
        buildIronMine()
        .then(() => {
          expect(stub.getCall(0)).to.be.calledWith("You need 5 stone to build something!");      
        })     
    })

    xit("Mine could be destroyed", ()=>{
        MapActions.buildStoneMine();      
        map.findIronMines().should('to.have.length', 1);

        MapActions.destroyMine();

        map.findIronMines().should('to.have.length', 0);
    })
    //todo add asserts
    it('Iron production path could be created', function () {
        MapActions.buildStoneMine();
        MapActions.buildStoneMine();
        MapActions.buildStoneMine();
        
        MapActions.buildCoalMine();
        MapActions.buildCoalMine();
        MapActions.buildCoalMine();

        MapActions.buildIronMine();
        MapActions.buildIronMine();
        MapActions.buildIronMine();
        
        MapActions.buildIronFactory();
        MapActions.buildIronFactory();
        MapActions.buildIronFactory();
        
        MapActions.buildSteelFactory();
        MapActions.buildSteelFactory();
        MapActions.buildSteelFactory();
         
        MapActions.buildSteelCasingFactory();
        MapActions.buildSteelCasingFactory();
        MapActions.buildSteelCasingFactory();

        MapActions.buildRocketShellFactory();
        MapActions.buildRocketShellFactory();
        MapActions.buildRocketShellFactory();
    })

    function buildIronMine(): Cypress.Chainable{
        map.findFirstIronTile().click();
        return MapActions.buildMineAction()
    }
});


