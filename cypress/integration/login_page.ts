const IRON_VEIN = "IronOre Icon";
const IRON_ORE = "IronOre";
const STONE = "Stone";
const EMPTY_PLACE = "None Icon";
const IRON_PLATE = "IronPlate";
const IRON_PLATE_FACTORY = "BuildIronPlateFactory";
const STEEL = "Steel";
const STEEL_FACTORY = "BuildSteelFactory";
const STEEL_CASING = "SteelCasing";
const STEEL_CASING_FACTORY = "BuildSteelCasingFactory";
const ROCKET_SHELL_FACTORY = "BuildRocketShellFactory";

context("Login page", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("list of available players", () => {
    const expectedPlayers = [
      "Nairobi",
      "Tokyo",
      "Berlin",
      "Denver",
      "Helsinki",
      "Rio",
      "Moscow",
      "Oslo",
      "Professor"
    ];

    getPlayersSelectValues().should("to.have.length", 9);
    getPlayersSelectValues()
      .then(options => Cypress.$.makeArray(options).map((i: any) => i.value))
      .should("deep.eq", expectedPlayers);
  });

  it("login  button - default player", function(){
    loginButton().should("contain", "Professor");
  })

  it("login  button - should display current player name", function(){
    loginButton().should("not.contain", "Oslo");

    selectPlayer("Oslo");
    loginButton().should("contain", "Oslo");
  })
});

function selectPlayer(player: string){
    getPlayerSelectCombobox().select(player);
}

function getPlayerSelectCombobox() {
  return cy.get("select");
}

function getPlayersSelectValues() {
  return cy.get("select > option");
}

function loginButton(){
    return cy.get('button')
}