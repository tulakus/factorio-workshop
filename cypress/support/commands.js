// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', (playerName, options = {}) => {
    cy.wait(2000); // game ticks like that
});

Cypress.Commands.add('stoneDeposit', (options = {}) => {
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('getBankDeposit', (sourceType, options = {}) => {
    return cy.get(`#bank-${sourceType}-amount`).invoke('text');
});

Cypress.Commands.add('findFirstStone', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Icon of Stone"]:first');
});

Cypress.Commands.add('findFirstOreVein', (veinType, options = {}) => {
    return cy.get(`td > div > img[alt="${veinType}"]:first`);
});

Cypress.Commands.add('findFirstAvailableSourceTile', (sourceType, options = {}) => {
    return cy.get(`td > div > img[alt="${sourceType}"]:first`);
});

Cypress.Commands.add('findMines', () => {
    return cy.get('td > div > img[alt="Icon of StoneMine"]');
});

Cypress.Commands.add('mineResource', (playerName, options = {}) => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', (playerName, options = {}) => {
    cy.get('#action-BuildMine').click();
});

Cypress.Commands.add('clickDialogAction', (objectName, options = {}) => {
    cy.get(`#action-${objectName}`).click();
});